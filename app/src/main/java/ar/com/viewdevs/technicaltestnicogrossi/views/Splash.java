package ar.com.viewdevs.technicaltestnicogrossi.views;

import android.content.Context;
import android.content.Intent;
import android.location.LocationListener;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import ar.com.viewdevs.technicaltestnicogrossi.R;

public class Splash extends AppCompatActivity
{

    public static Context contexto;
    ImageView imgSplash;
    TextView txtTienda;
    private double latitud = 0;
    private double longitud = 0;
    private LocationListener locationListener;
    private boolean yaEstoyAnimando = false;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        contexto = this;

        // 1 - INCIALIZO COMPONENTES UI:
        imgSplash = (ImageView) findViewById(R.id.imgSplash);
        txtTienda = (TextView) findViewById(R.id.txtTienda);

        // 2 - HAGO LA ANIMACION:
        animar();

    }

    @Override
    protected void onResume()
    {
        super.onResume();
        yaEstoyAnimando = false;
        animar();
    }

    public void animar()
    {
        //SONIDO:
        if(!yaEstoyAnimando)
        {
            yaEstoyAnimando = true;
            MediaPlayer mp = MediaPlayer.create(this, R.raw.reactive);
            mp.start();

            // 1 - ANIMACION LOGO:
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {

//                    Animation animationLogo = AnimationUtils.loadAnimation(Splash.contexto, R.anim.animacion_splash_icon);
                    Animation animationLogo = AnimationUtils.loadAnimation(Splash.contexto, R.anim.slide_in_animation);
                    imgSplash.startAnimation(animationLogo);

                    Animation animacionTexto = AnimationUtils.loadAnimation(Splash.contexto, R.anim.slide_in_animation_2);
                    txtTienda.startAnimation(animacionTexto);

                    // 2 - ESPERO A QUE LA SEGUNDA ANIMACION TERMINE:
                    animacionTexto.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation)
                        {
                            /*Intent aDondeTengoQueir;

                            //BUSCO LAS COORDENADAS GPS DEL CLIENTE, PARA AGREGAR A LOS CONTADORES:
                            Location location = intentaDarmeLaUltimaLocacion();

                            if (location != null) {
                                latitud = location.getLongitude();
                                longitud = location.getLatitude();


                            }
                            else
                            {
                                // NO PUDE CARGAR LA LOCATION, LO PONGO EN CERO CERO PARA QUE ARRANQUE LA APP:
                                latitud = -1;
                                longitud = -1;


                                //Toast.makeText(contexto, "DEBES ENCENDER LA UBICACION", Toast.LENGTH_SHORT).show();
                            }

                            System.out.println("Latitude" + String.valueOf(latitud));
                            System.out.println("Longitude" + String.valueOf(longitud));


                            if (MasterController.consultarConfig() != null) {
                                if (estoyEnLaUltimaVersion(longitud, latitud))
                                {
                                    // 1 - PRIMER VENTANA DE LA APP:
                                    aDondeTengoQueir = new Intent(Splash.contexto,CategoriasAct.class);
//                                  aDondeTengoQueir = new Intent(Splash.contexto,FormularioCargarRestaurantACT.class);
//                                  aDondeTengoQueir = new Intent(Splash.contexto,TestReciclerSucursales.class);

                                }
                                else
                                {
                                    // 2 - TENGO INTERNET , PERO REALMENTE TENGO UNA VERSION DESACTUALIZADA DEL APLICATIVO:
                                    aDondeTengoQueir = new Intent(Splash.contexto, AppDesactualizadaACT.class);
                                }
                                yaEstoyAnimando = false;
                                contexto.startActivity(aDondeTengoQueir);
                            } else {
                                // 3 - NO PUDE CONSULTAR LA CONFIG (POSIBLE PROBLEMA DE RED):
                                Toast.makeText(contexto, "Verifique su conexion a internet", Toast.LENGTH_SHORT).show();
                            }*/

                            Intent aDondeTengoQueir = new Intent(Splash.contexto,MainActivity.class);
                            contexto.startActivity(aDondeTengoQueir);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                }
            });
        }
    }

/*
    public boolean estoyEnLaUltimaVersion(double lang , double lat)
    {
        boolean ok = false;

        String url = MasterController.getFullURLForWS("getUltimaVersionEstable");

        try
        {
            List<ParametroJSON> parametros = new ArrayList<>();
            parametros.add(new ParametroJSON("lang",lang));
            parametros.add(new ParametroJSON("lat",lat));

            ThreadNetworkingPosta thread = new ThreadNetworkingPosta(url,true,parametros);
            thread.start();
            thread.join();

            if(thread.isOK())
            {

                PackageInfo packageInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
                String versionDelAplicativo = packageInfo.versionName;
                String ultimaVersionServer = (String) thread.dameResultado().getAsPrimitiva();

                if(versionDelAplicativo.equalsIgnoreCase(ultimaVersionServer))
                {
                    ok = true;
                }
                else
                {
                    ok = false;
                }
                System.out.println("LA ULTIMA VERSION DEL SERVER: " + ultimaVersionServer + "| TENGO LA " + versionDelAplicativo);

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return ok;
    }
    LocationManager mLocationManager;


    private Location intentaDarmeLaUltimaLocacion()
    {
        mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);

        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;

        for (String provider : providers)
        {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 2);
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 3);
                return null;
            }

            locationListener = new myLocationlistener();
            mLocationManager.requestLocationUpdates(provider, 1000, 0,locationListener);
            Location l = mLocationManager.getLastKnownLocation(provider);

            if (l == null)
            {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy())
            {
                bestLocation = l;
            }
        }

        return bestLocation;
    }


    private class myLocationlistener implements LocationListener
    {
        @Override
        public void onLocationChanged(Location location)
        {
            if(location != null)
            {
                latitud = location.getLatitude();
                longitud = location.getLongitude();
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    }*/
}
