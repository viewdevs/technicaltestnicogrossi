package ar.com.viewdevs.technicaltestnicogrossi.controllers;


import java.util.Date;


/**
 * Created by ngrossi on 7/7/2018.
 */

public class MasterController
{
    public static final String urlUserIDFlicker = "https://api.flickr.com/services/rest/?method=flickr.photos.getPopular&api_key=d65032c633016d54a16956788b082672&user_id=34492425%40N06&format=json&nojsoncallback=1&auth_token=72157671015034308-9e24247e0cb1f896&api_sig=f4716d114b7fb2cee4c8e8c4609228f3";
    public static String formatearFechaAAlgoBonito(Date fecha)
    {
        Date hoy = new Date();

        String fechaFormateada = "";

        int dia = fecha.getDate();
        int mes = fecha.getMonth() + 1 ;
        int year = fecha.getYear() + 1900;
        int hora = fecha.getHours();
        int minutos = fecha.getMinutes();

        String strDia;
        String strDiaSemana = "";
        String strMes;
        String strYear;
        String strH;
        String strM;


        //DIA:
        if(dia < 10)
        {
            strDia = "0" + dia;
        }
        else
        {
            strDia = "" + dia;
        }

        //MES:
        if(mes < 10)
        {
            strMes = "0" + mes;
        }
        else
        {
            strMes = "" + mes;
        }

        //YEAR:
        strYear = "" + year;

        //HORA:
        if(hora < 10)
        {
            strH = "0" + hora;
        }
        else
        {
            strH = "" + hora;
        }

        //MINUTOS:
        if(minutos < 10)
        {
            strM = "0" + minutos;
        }
        else
        {
            strM = "" + minutos;
        }

        switch (fecha.getDay())
        {
            case 0: strDiaSemana = "Lun"; break;
            case 1: strDiaSemana = "Mar"; break;
            case 2: strDiaSemana = "Mie"; break;
            case 3: strDiaSemana = "Jue"; break;
            case 4: strDiaSemana = "Vie"; break;
            case 5: strDiaSemana = "Sab"; break;
            case 6: strDiaSemana = "Dom"; break;

        }
        strDiaSemana = resuelveDiaDeLaSemana(fecha.getDay());

        if(dia == hoy.getDate() && (mes == hoy.getMonth() + 1))
        {
            fechaFormateada = strH + ":" + strM + " hs";
        }
        else if((mes == hoy.getMonth() + 1))
        {
            fechaFormateada = strDiaSemana + " " +  strDia + " (" + strH + ":" + strM + " hs)";
        }
        else
        {
            fechaFormateada =  strDia + "/" + strMes + "/" + strYear + " (" + strH + ":" + strM +" hs)";
        }


        return  fechaFormateada;
    }
    public static String resuelveDiaDeLaSemana(int dia)
    {
        String strDiaSemana = "";
        switch (dia)
        {
            case 0: strDiaSemana = "Lun"; break;
            case 1: strDiaSemana = "Mar"; break;
            case 2: strDiaSemana = "Mie"; break;
            case 3: strDiaSemana = "Jue"; break;
            case 4: strDiaSemana = "Vie"; break;
            case 5: strDiaSemana = "Sab"; break;
            case 6: strDiaSemana = "Dom"; break;

        }
        return strDiaSemana;
    }
    public static String resuelveStrMes(int mes)
    {
        String strMes = "";
        switch (mes)
        {
            case 1: strMes = "Enero"; break;
            case 2: strMes = "Febrero"; break;
            case 3: strMes = "Marzo"; break;
            case 4: strMes = "Abril"; break;
            case 5: strMes = "Mayo"; break;
            case 6: strMes = "Junio"; break;
            case 7: strMes = "Julio"; break;
            case 8: strMes = "Agosto"; break;
            case 9: strMes = "Septiembre"; break;
            case 10: strMes = "Octubre"; break;
            case 11: strMes = "Noviembre"; break;
            case 12: strMes = "Diciembre"; break;

        }
        return strMes;
    }

}
