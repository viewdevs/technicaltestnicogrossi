package ar.com.viewdevs.technicaltestnicogrossi.model;

import java.util.List;

/**
 * Created by ngrossi on 7/7/2018.
 */

public class ArrPhotosFlicker
{
    private int page;
    private int pages;
    private int perpage;
    private int total;
    private List<PhotoFlicker> photo;
    private String stat;

    public ArrPhotosFlicker()
    {
    }

    public ArrPhotosFlicker(int page, int pages, int perpage, int total, List<PhotoFlicker> photo, String stat)
    {
        this.page = page;
        this.pages = pages;
        this.perpage = perpage;
        this.total = total;
        this.photo = photo;
        this.stat = stat;
    }


     //GYS:
    public int getPage()
    {
        return page;
    }

    public void setPage(int page)
    {
        this.page = page;
    }

    public int getPages()
    {
        return pages;
    }

    public void setPages(int pages)
    {
        this.pages = pages;
    }

    public int getPerpage()
    {
        return perpage;
    }

    public void setPerpage(int perpage)
    {
        this.perpage = perpage;
    }

    public int getTotal()
    {
        return total;
    }

    public void setTotal(int total)
    {
        this.total = total;
    }

    public List<PhotoFlicker> getPhoto()
    {
        return photo;
    }

    public void setPhoto(List<PhotoFlicker> photo)
    {
        this.photo = photo;
    }

    public String getStat()
    {
        return stat;
    }

    public void setStat(String stat)
    {
        this.stat = stat;
    }

    @Override
    public String toString()
    {
        return "ArrPhotosFlicker{" +
                "page=" + page +
                ", pages=" + pages +
                ", perpage=" + perpage +
                ", total=" + total +
                ", photo=" + photo +
                ", stat='" + stat + '\'' +
                '}';
    }
}
