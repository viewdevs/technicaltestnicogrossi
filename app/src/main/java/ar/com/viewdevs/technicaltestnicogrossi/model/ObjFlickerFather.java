package ar.com.viewdevs.technicaltestnicogrossi.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ngrossi on 7/7/2018.
 */

public class ObjFlickerFather
{
    private ArrPhotosFlicker photos;

    public ObjFlickerFather()
    {
    }

    public ObjFlickerFather(ArrPhotosFlicker photos)
    {
        this.photos = photos;
    }

    public ArrPhotosFlicker getPhotos()
    {
        return photos;
    }

    public void setPhotos(ArrPhotosFlicker photos)
    {
        this.photos = photos;
    }

    @Override
    public String toString()
    {
        return "ObjFlickerFather{" +
                "photos=" + photos +
                '}';
    }


    //DYN:
    public List<String> dameFotos()
    {
        List<String> arrURLSFotos = new ArrayList<>();

        if(photos != null)
        {
            for(PhotoFlicker photoLoop : photos.getPhoto())
            {
                int farmID = photoLoop.getFarm();
                String serverID = photoLoop.getServer();
                long id = photoLoop.getId();
                String secret = photoLoop.getSecret();

                String urlFotoActual = "https://farm" + farmID + ".staticflickr.com/" + serverID + "/" + id + "_" + secret + ".jpg";
                arrURLSFotos.add(urlFotoActual);

            }
        }


        return arrURLSFotos;
    }
}
