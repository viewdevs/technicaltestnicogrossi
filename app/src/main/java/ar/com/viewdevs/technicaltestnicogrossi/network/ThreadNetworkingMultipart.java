package ar.com.viewdevs.technicaltestnicogrossi.network;


import android.graphics.Bitmap;


import java.io.File;
import java.util.ArrayList;
import java.util.List;



public class ThreadNetworkingMultipart extends Thread
{
    private String url;
    private List<ParametroJSON> parametros;
    private String resultado;

    public ThreadNetworkingMultipart(String url,List<ParametroJSON> parametros)
    {
        this.url = url;
        this.parametros = parametros;
    }

    @Override
    public void run()
    {
        super.run(); 

        if(url != null)
        {
            try
            {
                MultipartUtility multipart = new MultipartUtility(url, "UTF-8");

                // AGREGO PARAMETROS:
                for (ParametroJSON parametroLoop: parametros)
                {
                    multipart.addFilePart(parametroLoop.getNombreParametro(), (File) parametroLoop.getValor());
                }

                resultado = "";
                List<String> response = multipart.finish();
                System.out.println("RESPUESTA DE LA SOLICITUD MULTIPART:" + response);
                for (String line : response)
                {
                    System.out.println("Upload Files Response:::" + line);
                    resultado += line;
                }

            }catch (Exception e )
            {
                e.printStackTrace();
            }

        }
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }
}
