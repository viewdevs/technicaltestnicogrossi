package ar.com.viewdevs.technicaltestnicogrossi.views;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ar.com.viewdevs.technicaltestnicogrossi.R;
import ar.com.viewdevs.technicaltestnicogrossi.controllers.MasterController;
import ar.com.viewdevs.technicaltestnicogrossi.model.ObjFlickerFather;
import ar.com.viewdevs.technicaltestnicogrossi.network.ParametroJSON;
import ar.com.viewdevs.technicaltestnicogrossi.network.Retorno;
import ar.com.viewdevs.technicaltestnicogrossi.network.Task;
import ar.com.viewdevs.technicaltestnicogrossi.views.reciclerFotos.FotosAdapter;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    public static Context contexto;
    private Button btnBringPhotos;
    private TextView labelUltimaActualizacion;
    private ObjFlickerFather objFlickerFather;
    private RecyclerView recicler;
    private static FotosAdapter adapter;
    private Date ultimaActualizacion;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        contexto = this;

        // 1 - MATCH UI:
        btnBringPhotos = (Button) findViewById(R.id.btnBringPhotos); btnBringPhotos.setOnClickListener(this);
        recicler = (RecyclerView) findViewById(R.id.reciclerFotos);
        labelUltimaActualizacion = (TextView) findViewById(R.id.labelUltimaActualizacion);

        // 2 - POPULAR CON INFORMACION:
        List<String> listadoAux = new ArrayList<>();
        adapter = new FotosAdapter(listadoAux,this,recicler);

        recicler.setLayoutManager(new GridLayoutManager(contexto,2));
        recicler.setAdapter(adapter);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);


    }

    public void queryToFlicker()
    {
        //String url = MasterController.getFullURLForWS("getUltimaVersionEstable");
        String url = MasterController.urlUserIDFlicker;

        List<ParametroJSON> parametros = new ArrayList<>();
        /*parametros.add(new ParametroJSON("lang",latitud));
        parametros.add(new ParametroJSON("lat",longitud));*/

        Task task = new Task(url,true, new ArrayList<ParametroJSON>(), new Retorno()
        {
            @Override
            public void onProgress()
            {
                System.out.println("consultando..");
                //Toast.makeText(contexto,"Por favor espere.. ",Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onResult(String result)
            {
                //Toast.makeText(contexto,"resultado:" + resultado,Toast.LENGTH_SHORT).show();
                System.out.println("RESULTADO:" + result);

                if(result != null)
                {
                    objFlickerFather = new Gson().fromJson(result,ObjFlickerFather.class);
                    System.out.println("objFlickerFather: " + objFlickerFather.dameFotos().size());



                    for(String urlLoop : objFlickerFather.dameFotos())
                    {
                        System.out.println(urlLoop);
                    }

                    adapter.setListado(objFlickerFather.dameFotos());


                    ultimaActualizacion = new Date();
                    String labelPre = getResources().getString(R.string.label_pre);
                    String fechaBonito = MasterController.formatearFechaAAlgoBonito(ultimaActualizacion);
                    labelUltimaActualizacion.setText(labelPre + " " + fechaBonito );
                    progressBar.setVisibility(View.GONE);

                }


                //Toast.makeText(this,"RESULTADO: " + resultado,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancel()
            {
                //Toast.makeText(contexto, "Cancelado", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
        task.execute();
    }



    @Override
    public void onClick(View presionado)
    {
        if(presionado.getId() == btnBringPhotos.getId())
        {
            System.out.println("btnBringPhotos.click()");

            String url = MasterController.urlUserIDFlicker;

            queryToFlicker();

            //oast.makeText(this,respuesta,Toast.LENGTH_SHORT).show();
        }
    }
}
