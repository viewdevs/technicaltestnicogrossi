package ar.com.viewdevs.technicaltestnicogrossi.views.reciclerFotos;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import ar.com.viewdevs.technicaltestnicogrossi.R;
import ar.com.viewdevs.technicaltestnicogrossi.views.MainActivity;
import ar.com.viewdevs.technicaltestnicogrossi.views.dialogs.Dialogo;

public class FotosAdapter extends RecyclerView.Adapter<FotoViewHolder> /*implements View.OnClickListener*/
{

    private static List<String> listado;
    private MainActivity actPadre;
    private RecyclerView recyclerRecibido;

    public FotosAdapter(List<String> listado, MainActivity actPadre, RecyclerView recyclerRecibido)
    {

        // 1 - RECIBO LA ACTIVITY PADRE Y LA LISTA DE COSAS A CARGAR:
        this.listado = listado;
        this.actPadre = actPadre;
        this.recyclerRecibido = recyclerRecibido;
    }

    @Override
    public FotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        // 2 - SETEO LA VIEW A UN LAYOUT XML PARA CADA ITEM CON SU CORRESPONDIENTE LISTENER DE CLICK:
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.foto_view_holder, parent, false);
        //view.setOnClickListener(this);

        // 3 - ASOCIO UN VIEWHOLDER A LA VISTA CREADA EN EL PASO 2:
        FotoViewHolder viewHolder = new FotoViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(FotoViewHolder holder, final int position)
    {
        String actual = listado.get(position);

        System.out.println("posicion " + position + " - " + actual);


        //POPULO LA VISTA CON LOS DATOS DE LA ENTIDAD:
        Picasso.with(actPadre).load(actual).resize(360, 320).centerCrop().into(holder.foto);


        // ABRO CON EL CONTAINER:

        holder.contenedora.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                String selecionado = listado.get(position);

                    System.out.println("selecionado : " + selecionado);
                    Dialogo dialogo = new Dialogo(actPadre,selecionado);
                    //abrirCuadroDialogo(productoSeleccionado);
            }
        });
    }
    /*
    public void abrirCuadroDialogo(Producto productoSeleccionado)
    {
        System.out.println("ABRIENDO PRODUCTO: " + productoSeleccionado.toString());


        DialogoSeleccionarUnidad dialogoSeleccionarCantidad = new DialogoSeleccionarUnidad(actPadre, productoSeleccionado);
    }*/

    @Override
    public int getItemCount() {
            return listado.size();
        }

    public void setListado(List<String> listado)
    {
        this.listado = listado;
        this.notifyDataSetChanged();
    }
}
