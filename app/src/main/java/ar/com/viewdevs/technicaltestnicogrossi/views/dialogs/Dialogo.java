package ar.com.viewdevs.technicaltestnicogrossi.views.dialogs;

/**
 * Created by Nico-Gaming-Pc on 10/07/2018.
 */

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;


import com.squareup.picasso.Picasso;

import ar.com.viewdevs.technicaltestnicogrossi.R;
import ar.com.viewdevs.technicaltestnicogrossi.views.MainActivity;


@SuppressLint("ValidFragment")
public class Dialogo extends DialogFragment
{
    private String urlFotoSeleccionada;
    private View mView;
    private AlertDialog.Builder mBuilder;
    private AlertDialog dialogo;
    private MainActivity actPadre;
    private Context contexto;

    private ImageView img;

    public Dialogo(MainActivity actPadre, String urlFotoSeleccionada)
    {
        super();
        this.actPadre = actPadre;
        contexto = actPadre.contexto;
        this.urlFotoSeleccionada = urlFotoSeleccionada;


        // 1 - INICIALIZO EL LAYOUT DEL DIALOGO:
        LayoutInflater inflater = LayoutInflater.from(contexto);
        mView = inflater.inflate(R.layout.dialogo, null);
        mBuilder = new AlertDialog.Builder(contexto);

        // 2 - COMPONENTES UI:
        img = (ImageView) mView.findViewById(R.id.img);

        // INICIALIZO EL DIALOGO:
        mBuilder.setView(mView);
        dialogo = mBuilder.show();
        dialogo.setCancelable(true);
        if (dialogo != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialogo.getWindow().setLayout(width, height);
        }

        if(urlFotoSeleccionada != null)
        {
            Picasso.with(actPadre).load(urlFotoSeleccionada).resize(300, 600).centerCrop().into(img);
        }

    }


}

