package ar.com.viewdevs.technicaltestnicogrossi.network;


import java.util.ArrayList;
import java.util.List;


public class ThreadNetworkingPosta extends Thread
{
    private String url;
    private String token;
    private List<ParametroJSON> listadoParametros;
    private ResultadoRest resultado;
    private boolean isGetRequest /*, esperoArray*/;

    public ThreadNetworkingPosta(String url, boolean isGet)
    {
        this.url = url;
        this.isGetRequest = isGet;
        this.listadoParametros = new ArrayList<ParametroJSON>();
    }
    public ThreadNetworkingPosta(String url, boolean isGet, List<ParametroJSON> listadoParametros)
    {
        this.url = url;
        this.isGetRequest = isGet;
        this.listadoParametros = listadoParametros;
    }
    /*
    public ThreadNetworkingPosta(String url, String token, boolean isGet, List<ParametroJSON> listadoParametros)
    {
        this.url = url;
        this.token = token;
        this.isGetRequest = isGet;
        this.listadoParametros = listadoParametros;
    }
    */
    @Override
    public void run()
    {
        super.run(); 

        if(url != null)
        {
            if (isGetRequest)
            {
                System.out.println("SOY GET");
                resultado = JSONWS.sendPorGet(url, listadoParametros);
            } else
            {
                System.out.println("SOY POST");
                resultado = JSONWS.sendPorPost(url, listadoParametros);
            }
        }
    }
    public ResultadoRest dameResultado()
    {
        return resultado;
    }
    public boolean isOK()
    {
        boolean ok = false;

        if(resultado != null)
        {
            if (this.dameResultado().getAsReturnCode() == 200)
            {
                ok = true;
            }
        }

        return ok;
    }
    public int getCodigoError()
    {
        int codigoError = -1;

        if(resultado != null)
        {
            codigoError = dameResultado().getAsReturnCode();
        }

        return codigoError ;
    }
}
