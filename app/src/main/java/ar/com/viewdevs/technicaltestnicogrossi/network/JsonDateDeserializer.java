/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.viewdevs.technicaltestnicogrossi.network;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;
import java.util.Date;

/**
 *
 * @author ngrossi
 */
public class JsonDateDeserializer implements JsonDeserializer<Date> {
        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
           String s = json.getAsJsonPrimitive().getAsString();
           long l = Long.parseLong(s.substring(6, s.length() - 2));
           Date d = new Date(l);
           return d; 
        } 
     }
