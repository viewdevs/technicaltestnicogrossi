package ar.com.viewdevs.technicaltestnicogrossi.views.reciclerFotos;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import ar.com.viewdevs.technicaltestnicogrossi.R;


public class FotoViewHolder extends RecyclerView.ViewHolder
{
    public View contenedora;
    public ImageView foto;

    public FotoViewHolder(View itemView)
    {
        super(itemView);
        contenedora = (View) itemView.findViewById(R.id.contenedora);
        foto = (ImageView) itemView.findViewById(R.id.foto);
        //btnMasInfoProducto = (Button) itemView.findViewById(R.id.btnMasInfoProducto);
    }
}
