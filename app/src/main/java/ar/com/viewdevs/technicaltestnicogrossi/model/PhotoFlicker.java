package ar.com.viewdevs.technicaltestnicogrossi.model;

/**
 * Created by ngrossi on 7/7/2018.
 */

public class PhotoFlicker
{
    private long id;
    private String owner;
    private String secret;
    private String server;
    private int farm;
    private String title;
    private int ispublic;
    private int isfriend;
    private int isfamily;

    public PhotoFlicker()
    {
    }

    public PhotoFlicker(long id, String owner, String secret, String server, int farm, String title, int ispublic, int isfriend, int isfamily)
    {
        this.id = id;
        this.owner = owner;
        this.secret = secret;
        this.server = server;
        this.farm = farm;
        this.title = title;
        this.ispublic = ispublic;
        this.isfriend = isfriend;
        this.isfamily = isfamily;
    }

    //DYN:


    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getOwner()
    {
        return owner;
    }

    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public String getSecret()
    {
        return secret;
    }

    public void setSecret(String secret)
    {
        this.secret = secret;
    }

    public String getServer()
    {
        return server;
    }

    public void setServer(String server)
    {
        this.server = server;
    }

    public int getFarm()
    {
        return farm;
    }

    public void setFarm(int farm)
    {
        this.farm = farm;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public int getIspublic()
    {
        return ispublic;
    }

    public void setIspublic(int ispublic)
    {
        this.ispublic = ispublic;
    }

    public int getIsfriend()
    {
        return isfriend;
    }

    public void setIsfriend(int isfriend)
    {
        this.isfriend = isfriend;
    }

    public int getIsfamily()
    {
        return isfamily;
    }

    public void setIsfamily(int isfamily)
    {
        this.isfamily = isfamily;
    }
}
